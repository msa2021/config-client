package com.demo.microservices.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StaticConfigService {

	@Value("${bootcamp.profile}")
	private String profile;
	
	@Value("${bootcamp.comment}")
	private String comment;
	
	public Map<String, String> getConfig() {
		Map<String, String> map = new HashMap<>();
		
		map.put("profile", comment);
		map.put("comment", comment);
		
		return map;
	}
}
