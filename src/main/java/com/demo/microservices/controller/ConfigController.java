package com.demo.microservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.microservices.service.DynamicConfigService;
import com.demo.microservices.service.StaticConfigService;

import io.swagger.annotations.ApiOperation;

@RestController
public class ConfigController {

	private Environment environment;
	
	@Autowired
	private StaticConfigService staticConfigService;
	
	
	@Autowired
	private DynamicConfigService dynamicConfigService;
	
	public ConfigController(Environment environment) {
		this.environment = environment;
	}
	
	@ApiOperation("정적으로 config 정보를 loading 합니다")
	@GetMapping(value="/static")
	public Object getConfigFromStatic() {
		return staticConfigService.getConfig();
	}
	
	@ApiOperation("동적으로 config 정보를 refresh 합니다")
	@GetMapping(value="/dynamic")
	public Object getConfigFromDynamic() {
		return dynamicConfigService.getConfig();
	}
}
